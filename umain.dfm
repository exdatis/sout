object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Izlaz proizvoda u periodu po odeljenskim magacinima'
  ClientHeight = 566
  ClientWidth = 932
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 932
    Height = 41
    Align = alTop
    TabOrder = 0
    DesignSize = (
      932
      41)
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 53
      Height = 13
      Caption = 'Robu izdao'
    end
    object Label2: TLabel
      Left = 243
      Top = 12
      Width = 30
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Period'
    end
    object Label3: TLabel
      Left = 384
      Top = 14
      Width = 12
      Height = 13
      Caption = 'do'
    end
    object Label4: TLabel
      Left = 506
      Top = 14
      Width = 41
      Height = 13
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Proizvod'
    end
    object sbFindBy: TSpeedButton
      Left = 903
      Top = 8
      Width = 23
      Height = 22
      Cursor = crHandPoint
      Hint = 'Pronadji po ...'
      Anchors = [akLeft, akTop, akRight]
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FF314B62
        AC7D7EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF5084B20F6FE1325F8CB87E7AFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF32A0FE37A1FF
        106FE2325F8BB67D79FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF37A4FE379FFF0E6DDE355F89BB7F79FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        37A4FE359EFF0F6FDE35608BA67B7FFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF38A5FE329DFF156DCE444F5BFF
        00FF9C6B65AF887BAF887EAA8075FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF3BABFFA1CAE7AD8679A98373E0CFB1FFFFDAFFFFDDFCF8CFCCB2
        9FA1746BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC0917DFC
        E9ACFFFFCCFFFFCFFFFFD0FFFFDEFFFFFAE3D3D1996965FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFB08978FAD192FEF4C2FFFFD0FFFFDAFFFFF6FFFF
        FCFFFFFCB69384FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB08978FEDA97ED
        B478FBEEBBFFFFD3FFFFDCFFFFF4FFFFF4FFFFE2E9DDBCA67B73FF00FFFF00FF
        FF00FFFF00FFFF00FFB18A78FFDE99E9A167F4D199FEFCCCFFFFD5FFFFDAFFFF
        DCFFFFD7EFE6C5A97E75FF00FFFF00FFFF00FFFF00FFFF00FFAA7F73FAE0A4F0
        B778EEBA7BF6DDA6FEFBCCFFFFD3FFFFD1FFFFD7D9C5A7A3756CFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFCEB293FFFEDDF4D1A5EEBA7BF2C78FF8E1ABFCF0
        BAFCFACAA3776FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA1746BE1
        D4D3FFFEEEF7CC8CF0B473F7C788FCE3A5C2A088A5776CFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FF986865BA9587EAD7A4EAD59EE0C097A577
        6CA5776CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFA77E70A98073A4786EFF00FFFF00FFFF00FFFF00FF}
      ParentShowHint = False
      ShowHint = True
      OnClick = sbFindByClick
    end
    object cmbSupplier: TComboBox
      Left = 64
      Top = 9
      Width = 177
      Height = 21
      Cursor = crHandPoint
      AutoDropDown = True
      AutoCloseUp = True
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 5
      TabOrder = 0
      Text = 'Izaberite magacin'
    end
    object dtpMin: TDateTimePicker
      Left = 277
      Top = 9
      Width = 105
      Height = 21
      Cursor = crHandPoint
      Anchors = [akLeft, akTop, akRight]
      Date = 42147.452825787040000000
      Time = 42147.452825787040000000
      TabOrder = 1
    end
    object dtpMax: TDateTimePicker
      Left = 399
      Top = 9
      Width = 105
      Height = 21
      Cursor = crHandPoint
      Anchors = [akLeft, akTop, akRight]
      Date = 42147.454237268520000000
      Time = 42147.454237268520000000
      TabOrder = 2
    end
    object edtFindProduct: TEdit
      Left = 551
      Top = 9
      Width = 347
      Height = 21
      Hint = 'Deo naziva +Enter'
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Text = 'Pronadji...'
      OnEnter = edtFindProductEnter
      OnExit = edtFindProductExit
      OnKeyPress = edtFindProductKeyPress
    end
  end
  object statusBarMain: TStatusBar
    Left = 0
    Top = 547
    Width = 932
    Height = 19
    AutoHint = True
    Panels = <>
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 932
    Height = 45
    Align = alTop
    Caption = ' Izabrani proizvod '
    TabOrder = 2
    DesignSize = (
      932
      45)
    object dbId: TDBEdit
      Left = 8
      Top = 16
      Width = 89
      Height = 21
      Cursor = crHandPoint
      Hint = 'ID broj'
      Anchors = [akLeft, akTop, akRight]
      Color = clSilver
      DataField = 'art_id'
      DataSource = dsProduct
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
    end
    object dbCode: TDBEdit
      Left = 99
      Top = 16
      Width = 121
      Height = 21
      Cursor = crHandPoint
      Hint = 'Sifra'
      Anchors = [akLeft, akTop, akRight]
      Color = clSilver
      DataField = 'art_sifra'
      DataSource = dsProduct
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
    end
    object dbProduct: TDBEdit
      Left = 222
      Top = 16
      Width = 282
      Height = 21
      Cursor = crHandPoint
      Hint = 'Proizvod'
      Anchors = [akLeft, akTop, akRight]
      Color = clSilver
      DataField = 'art_naziv'
      DataSource = dsProduct
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
    end
    object dbMeasure: TDBEdit
      Left = 506
      Top = 16
      Width = 127
      Height = 21
      Cursor = crHandPoint
      Hint = 'Mera'
      Anchors = [akLeft, akTop, akRight]
      Color = clSilver
      DataField = 'jm_naziv'
      DataSource = dsProduct
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
    end
    object dbGroup: TDBEdit
      Left = 635
      Top = 16
      Width = 291
      Height = 21
      Cursor = crHandPoint
      Hint = 'Grupa'
      Color = clSilver
      DataField = 'ag_naziv'
      DataSource = dsProduct
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 4
    end
  end
  object dbgReport: TDBGrid
    Left = 1
    Top = 87
    Width = 344
    Height = 434
    Cursor = crHandPoint
    Hint = 'Klikni naslov za sortiranje'
    Color = clSilver
    DataSource = dsReport
    DrawingStyle = gdsGradient
    GradientEndColor = clSilver
    GradientStartColor = clGray
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    ShowHint = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBNavReport: TDBNavigator
    Left = 3
    Top = 522
    Width = 248
    Height = 25
    Cursor = crHandPoint
    DataSource = dsReport
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    Flat = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
  end
  object btnQuit: TButton
    Left = 855
    Top = 522
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Action = acQuitApp
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
  end
  object btnRunQuery: TButton
    Left = 257
    Top = 522
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Action = acRunQueryReport
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
  end
  object DBChart1: TDBChart
    Left = 351
    Top = 87
    Width = 579
    Height = 434
    Title.Text.Strings = (
      'Izdvanje proizvoda po magacinima')
    View3DOptions.Elevation = 315
    View3DOptions.Orthogonal = False
    View3DOptions.Perspective = 0
    View3DOptions.Rotation = 360
    TabOrder = 7
    ColorPaletteIndex = 13
    object flowPanelProducts: TFlowPanel
      Left = 199
      Top = 2
      Width = 347
      Height = 333
      TabOrder = 0
      Visible = False
      object dbgProducts: TDBGrid
        Left = 1
        Top = 1
        Width = 344
        Height = 306
        Cursor = crHandPoint
        Hint = 'SPACE taster za Ok'
        Color = clSilver
        DataSource = dsProduct
        DrawingStyle = gdsGradient
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnKeyPress = dbgProductsKeyPress
      end
      object btnOk: TButton
        Left = 1
        Top = 307
        Width = 171
        Height = 25
        Cursor = crHandPoint
        Hint = 'Izaberi proizvod'
        Align = alBottom
        Caption = 'Izaberi ovaj proizvod'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnOkClick
      end
      object btncancel: TButton
        Left = 172
        Top = 307
        Width = 170
        Height = 25
        Cursor = crHandPoint
        Hint = 'Ponovi pretragu'
        Caption = 'Ponovi pretragu'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btncancelClick
      end
    end
    object Series1: TPieSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = True
      DataSource = qReport
      XLabelsSource = 'm_naziv'
      XValues.Order = loAscending
      YValues.Name = 'Pie'
      YValues.Order = loNone
      YValues.ValueSource = 'sum'
      Frame.InnerBrush.BackColor = clRed
      Frame.InnerBrush.Gradient.EndColor = clGray
      Frame.InnerBrush.Gradient.MidColor = clWhite
      Frame.InnerBrush.Gradient.StartColor = 4210752
      Frame.InnerBrush.Gradient.Visible = True
      Frame.MiddleBrush.BackColor = clYellow
      Frame.MiddleBrush.Gradient.EndColor = 8553090
      Frame.MiddleBrush.Gradient.MidColor = clWhite
      Frame.MiddleBrush.Gradient.StartColor = clGray
      Frame.MiddleBrush.Gradient.Visible = True
      Frame.OuterBrush.BackColor = clGreen
      Frame.OuterBrush.Gradient.EndColor = 4210752
      Frame.OuterBrush.Gradient.MidColor = clWhite
      Frame.OuterBrush.Gradient.StartColor = clSilver
      Frame.OuterBrush.Gradient.Visible = True
      Frame.Visible = False
      Frame.Width = 4
      OtherSlice.Legend.Visible = False
    end
  end
  object btnSaveChart: TButton
    Left = 334
    Top = 522
    Width = 107
    Height = 25
    Cursor = crHandPoint
    Hint = 'Sa'#269'uvaj grafikon kao BMP image'
    Caption = 'Sa'#269'uvaj grafikon'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 8
    OnClick = btnSaveChartClick
  end
  object btnExportToCsv: TButton
    Left = 443
    Top = 522
    Width = 104
    Height = 25
    Cursor = crHandPoint
    Action = acExportToExcel
    ImageAlignment = iaRight
    Images = ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
  end
  object dbConnection: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=edh20' +
      '15'
    LoginPrompt = False
    Left = 104
    Top = 104
  end
  object qStorages: TADOQuery
    Connection = dbConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'seleCT'
      '  M_ID,'#9
      '  M_NAZIV'
      'FROM '
      '  MAGACIN'
      'ORDER BY'
      '  M_ID')
    Left = 240
    Top = 232
    object qStoragesm_id: TAutoIncField
      FieldName = 'm_id'
      ReadOnly = True
    end
    object qStoragesm_naziv: TWideStringField
      FieldName = 'm_naziv'
      Size = 50
    end
  end
  object dsStorages: TDataSource
    DataSet = qStorages
    Left = 280
    Top = 248
  end
  object actionListMain: TActionList
    Images = ImageList1
    Left = 416
    Top = 232
    object acQuitApp: TAction
      Caption = 'Zatvori'
      Hint = 'Zatvori aplikaciju'
      ShortCut = 16465
      OnExecute = acQuitAppExecute
    end
    object acFindByName: TAction
      Caption = 'Pronadji naziv'
      Hint = 'Pronadji po nazivu proizvoda'
      ShortCut = 49230
      OnExecute = acFindByNameExecute
    end
    object acFindByCode: TAction
      Caption = 'Pronadji sifru'
      ShortCut = 49219
      OnExecute = acFindByCodeExecute
    end
    object acRunQueryReport: TAction
      Caption = 'Pokreni upit'
      Hint = 'Pokreni izvestaj'
      ShortCut = 49234
      OnExecute = acRunQueryReportExecute
    end
    object acExportToExcel: TAction
      Caption = 'Export CSV'
      Hint = 'Eksportuj u csv format'
      ImageIndex = 0
      OnExecute = acExportToExcelExecute
    end
  end
  object qProduct: TADOQuery
    Connection = dbConnection
    CursorType = ctStatic
    AfterOpen = qProductAfterOpen
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM VPRODUCT')
    Left = 240
    Top = 328
    object qProductart_id: TIntegerField
      Alignment = taLeftJustify
      DisplayLabel = 'ID'
      DisplayWidth = 5
      FieldName = 'art_id'
    end
    object qProductart_sifra: TWideStringField
      DisplayLabel = 'Sifra'
      DisplayWidth = 7
      FieldName = 'art_sifra'
      Size = 30
    end
    object qProductart_naziv: TWideStringField
      DisplayWidth = 17
      FieldName = 'art_naziv'
      Size = 70
    end
    object qProductjm_naziv: TWideStringField
      FieldName = 'jm_naziv'
      Visible = False
      Size = 15
    end
    object qProductjm_oznaka: TWideStringField
      DisplayLabel = 'JM'
      DisplayWidth = 5
      FieldName = 'jm_oznaka'
      Size = 7
    end
    object qProductag_naziv: TWideStringField
      DisplayLabel = 'Grupa'
      FieldName = 'ag_naziv'
      Size = 30
    end
  end
  object dsProduct: TDataSource
    DataSet = qProduct
    Left = 280
    Top = 344
  end
  object qReport: TADOQuery
    Connection = dbConnection
    CursorType = ctStatic
    BeforeOpen = qReportBeforeOpen
    AfterOpen = qReportAfterOpen
    AfterClose = qReportAfterClose
    Parameters = <
      item
        Name = 'supplier'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'date_min'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'date_max'
        DataType = ftDateTime
        Size = -1
        Value = Null
      end
      item
        Name = 'product'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  B.m_naziv,'
      '  C.jm_naziv,'
      '  sum(A.ka_minus) as sum'
      'FROM'
      '  kartica_a A,'
      '  magacin B,'
      '  vproduct C'
      'WHERE'
      '  B.m_id = A.magacin_saradnik'
      '  AND'
      '  A.ka_magacin = :supplier'
      '  AND'
      '  A.ka_datum >= :date_min'
      '  AND'
      '  A.ka_datum <= :date_max'
      '  AND'
      '  A.ka_artikal = :product'
      '  AND'
      '  C.art_id = A.ka_artikal'
      'GROUP BY'
      '  B.m_naziv,'
      '  C.jm_naziv'
      'ORDER BY'
      '  sum DESC')
    Left = 392
    Top = 400
    object qReportm_naziv: TWideStringField
      DisplayLabel = 'Odeljenje(magacin)'
      DisplayWidth = 25
      FieldName = 'm_naziv'
      ReadOnly = True
      Size = 50
    end
    object qReportjm_naziv: TWideStringField
      DisplayLabel = 'Mera'
      DisplayWidth = 9
      FieldName = 'jm_naziv'
      ReadOnly = True
      Size = 15
    end
    object qReportsum: TBCDField
      DisplayLabel = 'Izdato'
      DisplayWidth = 9
      FieldName = 'sum'
      ReadOnly = True
      DisplayFormat = '#0.00'
      Precision = 28
      Size = 6
    end
  end
  object dsReport: TDataSource
    DataSet = qReport
    Left = 440
    Top = 416
  end
  object PopupMenu1: TPopupMenu
    Left = 536
    Top = 168
    object Pronadjisifru1: TMenuItem
      Action = acFindByCode
    end
    object Pronadjinaziv1: TMenuItem
      Action = acFindByName
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.bmp'
    FileName = 'grafikon'
    Filter = 'bmp|*.bmp;*.BMP'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Sa'#269'uvaj grafikon'
    Left = 208
    Top = 144
  end
  object ImageList1: TImageList
    Left = 88
    Top = 216
    Bitmap = {
      494C010101000800140010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007374
      7499888A89AE868887AC848685AA828483A8808281A67F8180A57D7F7EA37C7E
      7DA2737575A10303030600000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A9AB
      AACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFCFD0D0EE0809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000005110A20164C307C164C307C6FA7
      8AE99ACFB3FF9ACFB3FF9ACFB3FF9ACFB3FFC4E0D1FFFFFFFFFFFFFFFFFFFDFD
      FDFFCDCFCFEF0809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B3AA96FFC7CC39EFF7BC3
      9EFF7AC29DFF78C29CFF78C19CFF56B584FF6EB892FFE2E2E2FFE2E2E2FFFAFA
      FAFFCDCECDF00809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B5CB486FCEEF2EFFFE6EC
      E9FFEDF0EEFFBBDBCAFFA6D2BBFF9BCEB3FF70BC94FFEBEBEBFFF9F9F9FFF6F7
      F7FFCBCDCCF10809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B5DB586FC8CCAAAFF10A0
      55FF16A159FF0C9E51FF56B283FF9BCEB3FF6FBB94FFE8E9E9FFF3F4F4FFF3F4
      F4FFC9CBCBF20809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B5DB587FCFFFFFFFF6EBE
      94FF31AB6DFF3DAC72FFA5D3BAFF9BCEB3FF6CB791FFDDDEDEFFE5E6E6FFF0F1
      F1FFC8CACAF30809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B5EB488FB99CDB1FF28A8
      67FF49A976FF0E9D53FF8ECBABFF9BCEB3FF69B58EFFD7D7D7FFD6D8D8FFECEE
      EEFFCACDCDF30809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B5EB489FBCAE2D5FFB2D8
      C3FFFEFEFEFFBEDCCBFFC8E3D5FF9BCEB3FF70BC95FFEDEFEFFFEFF0F0FFF4F5
      F5FFD0D2D2F30809091000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D35205B41AB75FC88C7A6FF88C7
      A6FF88C7A6FF88C7A6FF88C7A6FF60B88AFF6FBC94FFF2F3F3FFE7E8E7FFE3E4
      E3FFA3A7A4F20909091200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000006160E2A2667459E2768469F68A9
      88F082C3A2FF82C3A2FF82C2A0FF82C3A2FFB0D4C1FFFBFBFBFFC4C7C5FFBBBC
      BBFF7C807ECE0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A9AB
      AADBE4E5E5FFE3E5E5FFE9EAEAFFEEEFEEFFF3F3F3FFE5E7E6FFA6AAA8F88386
      84DC5B5B5B5F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001919
      192B292A2A44292A2A442A2B2A44292B2A44282A294427292844111313220000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object SaveDialog2: TSaveDialog
    DefaultExt = '*.csv'
    FileName = 'report'
    Filter = 'csv|*.csv;*.CSV|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export tabele u CSV format'
    Left = 112
    Top = 376
  end
  object qGeneral: TADOQuery
    Connection = dbConnection
    Parameters = <>
    Left = 56
    Top = 304
  end
end
