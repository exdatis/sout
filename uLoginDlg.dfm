object dlgLogin: TdlgLogin
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Konekcija sa bazom podataka'
  ClientHeight = 153
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 2
    Width = 402
    Height = 105
    Caption = ' Podaci korisnika '
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 36
      Height = 13
      Caption = 'Korisnik'
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 35
      Height = 13
      Caption = 'Lozinka'
    end
    object Label3: TLabel
      Left = 16
      Top = 72
      Width = 57
      Height = 13
      Caption = 'Radna baza'
    end
    object edtUser: TEdit
      Left = 88
      Top = 21
      Width = 305
      Height = 21
      TabOrder = 0
    end
    object edtPwd: TEdit
      Left = 88
      Top = 45
      Width = 305
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object cmbDb: TComboBox
      Left = 88
      Top = 69
      Width = 305
      Height = 21
      TabOrder = 2
      Text = 'Izaberite bazu podataka'
    end
  end
  object btnOk: TButton
    Left = 250
    Top = 113
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 328
    Top = 113
    Width = 75
    Height = 25
    Cursor = crHandPoint
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
