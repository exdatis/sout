unit uLoginDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TdlgLogin = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtUser: TEdit;
    Label2: TLabel;
    edtPwd: TEdit;
    Label3: TLabel;
    cmbDb: TComboBox;
    btnOk: TButton;
    btnCancel: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgLogin: TdlgLogin;

implementation

{$R *.dfm}

end.
