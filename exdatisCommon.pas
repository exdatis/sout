unit exdatisCommon;


interface
uses
  Registry, Classes, Windows;
procedure getODBCsources(var odbcList : TStringList);
function getConnectionString(const currSource : String) : String;
implementation

procedure getODBCsources(var odbcList : TStringList);
var
   Reg : TRegistry;
begin 
   Reg := TRegistry.Create;
   try 
    Reg.RootKey := HKEY_CURRENT_USER;
    Reg.LazyWrite := false;
    Reg.OpenKey('Software\ODBC\ODBC.INI\ODBC Data Sources', false);
    Reg.GetValueNames(odbcList);
    Reg.CloseKey;
   finally 
        Reg.Free; 
   end;
end;
function getConnectionString(const currSource : String) : String;
var
  connString : String;
begin
  connString:= 'Provider=MSDASQL.1;Persist Security Info=False;Data Source=' ;
  connString:= connString + currSource + ';';
  Result:= connString;
end;
end.
