﻿unit umain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ComCtrls, StdCtrls, ExtCtrls, Buttons, ActnList, DBCtrls,
  Mask, Grids, DBGrids, Menus, System.Actions, VCLTee.TeEngine, VCLTee.TeeProcs,
  VCLTee.Chart, VCLTee.DBChart, VCLTee.Series, Vcl.Imaging.JPEG, Vcl.ImgList,
  ShellApi;

type
  TfrmMain = class(TForm)
    dbConnection: TADOConnection;
    qStorages: TADOQuery;
    qStoragesm_id: TAutoIncField;
    qStoragesm_naziv: TWideStringField;
    dsStorages: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    cmbSupplier: TComboBox;
    Label2: TLabel;
    dtpMin: TDateTimePicker;
    Label3: TLabel;
    dtpMax: TDateTimePicker;
    Label4: TLabel;
    edtFindProduct: TEdit;
    sbFindBy: TSpeedButton;
    actionListMain: TActionList;
    acQuitApp: TAction;
    acFindByName: TAction;
    acFindByCode: TAction;
    statusBarMain: TStatusBar;
    qProduct: TADOQuery;
    qProductart_id: TIntegerField;
    qProductart_sifra: TWideStringField;
    qProductart_naziv: TWideStringField;
    qProductjm_naziv: TWideStringField;
    qProductjm_oznaka: TWideStringField;
    qProductag_naziv: TWideStringField;
    dsProduct: TDataSource;
    GroupBox1: TGroupBox;
    dbId: TDBEdit;
    dbCode: TDBEdit;
    dbProduct: TDBEdit;
    dbMeasure: TDBEdit;
    dbGroup: TDBEdit;
    qReport: TADOQuery;
    qReportm_naziv: TWideStringField;
    qReportjm_naziv: TWideStringField;
    qReportsum: TBCDField;
    dsReport: TDataSource;
    dbgReport: TDBGrid;
    DBNavReport: TDBNavigator;
    btnQuit: TButton;
    btnRunQuery: TButton;
    acRunQueryReport: TAction;
    PopupMenu1: TPopupMenu;
    Pronadjisifru1: TMenuItem;
    Pronadjinaziv1: TMenuItem;
    DBChart1: TDBChart;
    Series1: TPieSeries;
    flowPanelProducts: TFlowPanel;
    dbgProducts: TDBGrid;
    btnOk: TButton;
    btncancel: TButton;
    SaveDialog1: TSaveDialog;
    btnSaveChart: TButton;
    ImageList1: TImageList;
    acExportToExcel: TAction;
    btnExportToCsv: TButton;
    SaveDialog2: TSaveDialog;
    procedure edtFindProductEnter(Sender: TObject);
    procedure edtFindProductExit(Sender: TObject);
    procedure acQuitAppExecute(Sender: TObject);
    procedure acFindByNameExecute(Sender: TObject);
    procedure qProductAfterOpen(DataSet: TDataSet);
    procedure acFindByCodeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qReportBeforeOpen(DataSet: TDataSet);
    procedure acRunQueryReportExecute(Sender: TObject);
    procedure sbFindByClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btncancelClick(Sender: TObject);
    procedure edtFindProductKeyPress(Sender: TObject; var Key: Char);
    procedure dbgProductsKeyPress(Sender: TObject; var Key: Char);
    procedure qReportAfterOpen(DataSet: TDataSet);
    procedure btnSaveChartClick(Sender: TObject);
    procedure qReportAfterClose(DataSet: TDataSet);
    procedure acExportToExcelExecute(Sender: TObject);
  private
    { Private declarations }
    procedure runQueryProduct(const paramName : String);
    procedure runQueryReport;
  public
    { Public declarations }
    procedure fillStorages;
  end;

var
  frmMain: TfrmMain;
const
  SQL_PRODUCT : String = 'SELECT * FROM VPRODUCT ';

implementation
uses
  uLoginDlg, exdatisCommon;
{$R *.dfm}

procedure TfrmMain.acExportToExcelExecute(Sender: TObject);
var
  csvFile : String;
  fullList, recList : TStringList;
  tempString, currString, delimiterString : String;
  currPosition : TBookmark;
begin
  if not qReport.Active then
    Exit;
  if qReport.IsEmpty then
    Exit;
  {pitaj gde da se sacuva}
  if SaveDialog2.Execute then
    csvFile:= SaveDialog2.FileName;
  if Length(csvFile) < 5 then
    Exit;

  {pokupi rezultate}
  fullList:= TStringList.Create;
  {odredi delimiter karakter}
  delimiterString:= InputBox('List separator', 'Unesite separator slogova: ', ';');
  qReport.DisableControls;
  {save position}
  currPosition:= qReport.GetBookmark;
  qReport.First;
  {naslov}
  currString:= qProduct.FieldByName('art_naziv').AsString + delimiterString;
  currString:= currString + cmbSupplier.Text + delimiterString;
  //kreiraj string period
  tempString:= FormatDateTime('dd.MM.yyyy', dtpMin.Date) + ' - ';
  tempString:= tempString + FormatDateTime('dd.MM.yyyy', dtpMax.Date);
  currString:= currString + tempString;
  //add to fullList
  fullList.Append(currString);
  //loop
  while not qReport.Eof do
  begin
    currString:= qReport.FieldByName('m_naziv').AsString + delimiterString;
    currString:= currString + qReport.FieldByName('jm_naziv').AsString + delimiterString;
    currString:= currString + FormatFloat('#0.00', qReport.FieldByName('sum').AsFloat);
    //add to full_list
    fullList.Append(currString);
    qReport.Next
  end;
  {enable controls}
  qReport.GotoBookmark(currPosition);

  qReport.EnableControls;
  //save to file
  fullList.SaveToFile(csvFile);
  //free all
  fullList.Free;
  //showmessage
  ShowMessage('Fajl je sačuvan, otvaram datoteku.');
  //otvori
  Screen.Cursor:= crHourGlass;
  try
    ShellExecute(Handle, 'open', pChar(csvFile),nil,nil,SW_SHOWNORMAL) ;
  finally
    Screen.Cursor:= crDefault;
  end;
end;

procedure TfrmMain.acFindByCodeExecute(Sender: TObject);
var
  newSql, paramName : String;
begin
  {zatvori upit i ocisti sql text}
  qProduct.DisableControls;
  qProduct.SQL.Clear;
  {najpre napravi uslov za upit}
  newSql:= 'WHERE LOWER(ART_SIFRA) LIKE LOWER(:ART_SIFRA) ORDER BY ART_SIFRA';
  qProduct.SQL.Text:= SQL_PRODUCT + newSql;
  paramName:= 'ART_SIFRA';
  runQueryProduct(paramName);
end;

procedure TfrmMain.acFindByNameExecute(Sender: TObject);
var
  newSql, paramName : String;
begin
  {zatvori upit i ocisti sql text}
  qProduct.DisableControls;
  qProduct.SQL.Clear;
  {najpre napravi uslov za upit}
  newSql:= 'WHERE LOWER(ART_NAZIV) LIKE LOWER(:ART_NAZIV) ORDER BY ART_NAZIV';
  qProduct.SQL.Text:= SQL_PRODUCT + newSql;
  paramName:= 'ART_NAZIV';
  runQueryProduct(paramName);
end;

procedure TfrmMain.acQuitAppExecute(Sender: TObject);
begin
  self.Close;
  Application.Terminate;
end;

procedure TfrmMain.acRunQueryReportExecute(Sender: TObject);
begin
  runQueryReport;
  btnRunQuery.Enabled:= False;
end;

procedure TfrmMain.btncancelClick(Sender: TObject);
begin
  qProduct.DisableControls;
  qProduct.Close;
  flowPanelProducts.Visible:= False;
  edtFindProduct.SetFocus;
end;

procedure TfrmMain.btnOkClick(Sender: TObject);
begin
  {omoguci izvrsavanje upita}
  btnRunQuery.Enabled:= True;
  flowPanelProducts.Visible:= False;
  btnRunQuery.SetFocus;
  btnRunQuery.Click;
  Application.ProcessMessages;
end;

procedure TfrmMain.btnSaveChartClick(Sender: TObject);
var
  chartBmpFile : String;
begin
  if SaveDialog1.Execute then
    chartBmpFile:= SaveDialog1.FileName;
  //ShowMessage(chartBmpFile);
  if Length(chartBmpFile) > 5 then
  begin
    try
    DBChart1.SaveToBitmapFile(chartBmpFile);
    except
    on e : Exception do
      begin
        ShowMessageUser(e.Message);
        Exit;
      end;
    end;
    ShowMessage('Grafikon je sačuvan.');
  end;
end;

procedure TfrmMain.dbgProductsKeyPress(Sender: TObject; var Key: Char);
begin
  if(Key = #32) then
    btnOk.Click;
end;

procedure TfrmMain.edtFindProductEnter(Sender: TObject);
begin
  {promeni font color i obrisi text}
  edtFindProduct.Text:= '';
  edtFindProduct.Font.Color:= clBlack;
end;

procedure TfrmMain.edtFindProductExit(Sender: TObject);
begin
  {promeni font color i tekst}
  edtFindProduct.Font.Color:= clGray;

   { TODO 1 -oexdatis -cizmena dogadjaja : Ovo treba iskoristiti posle otvaranja query-ija }
   {odradjeno, zadrzo sam izmenu font-color a tekst menjam posle otvaranja upita}
end;

procedure TfrmMain.edtFindProductKeyPress(Sender: TObject; var Key: Char);
begin
  if(Key = #13) then
    acFindByName.Execute;
end;

procedure TfrmMain.fillStorages;
begin
  {pronadji magacine i napuni comboBox}
  try
    qStorages.Open;
  except
    on e : Exception do
    begin
      qStorages.CancelUpdates;
      qStorages.Close;
      ShowMessage(e.Message);
      Exit;
    end;
  end;
  {popuni combo}
  qStorages.First;
  qStorages.DisableControls;
  {clear comboBox}
  cmbSupplier.Clear;
  while(not qStorages.Eof) do
  begin
    cmbSupplier.Items.Append(qStorages.Fields.FieldByName('M_NAZIV').AsString);
    qStorages.Next;
  end;
  {oslobodi kontrole}
  qStorages.EnableControls;
  cmbSupplier.Text:= 'Izaberite magacin';
  cmbSupplier.SetFocus;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  newDlg : TdlgLogin;
  listOfSources : TStringList;
begin
  {provera korisnika}

  Screen.Cursor:= crSQLWait;
  try
    fillStorages;
  finally
    Screen.Cursor:= crDefault;
  end;
end;

procedure TfrmMain.qProductAfterOpen(DataSet: TDataSet);
begin
  {promeni tekst}
  edtFindProduct.Text:= 'Pronadji...';
end;

procedure TfrmMain.qReportAfterClose(DataSet: TDataSet);
begin
  {Kada nema upita, ocisti naslov chart-a}
  DBChart1.Title.Clear;
end;

procedure TfrmMain.qReportAfterOpen(DataSet: TDataSet);
var
  newTitle : String;
begin
  DBChart1.Title.Clear;
  newTitle:= qProduct.FieldByName('art_naziv').AsString + '(Jedinica mere: ' ;
  newTitle:= newTitle + qProduct.FieldByName('jm_naziv').AsString + ')';
  newTitle:= newTitle + #13#10;
  newTitle:= newTitle + 'Izdato u periodu:' + FormatDateTime('dd.MM.yyyy', dtpMin.Date);
  newTitle:= newTitle + ' - ' + FormatDateTime('dd.MM.yyyy', dtpMax.Date);
  newTitle:= newTitle + ' Snabdevač: ' + cmbSupplier.Text;
  DBChart1.Title.Text.Text:= newTitle;
end;

procedure TfrmMain.qReportBeforeOpen(DataSet: TDataSet);
begin
  {setuj vrednosti parametara}
  qReport.Parameters.ParamByName('SUPPLIER').Value:= qStorages.FieldByName('m_id').AsInteger;
  qReport.Parameters.ParamByName('DATE_MIN').Value:= dtpMin.Date;
  qReport.Parameters.ParamByName('DATE_MAX').Value:= dtpMax.Date;
  qReport.Parameters.ParamByName('PRODUCT').Value:= qProduct.FieldByName('art_id').AsInteger;
end;

procedure TfrmMain.runQueryProduct(const paramName : String);
const
  EMPTY_SET_ERROR : String = 'Prazan skup podataka.';
var
  paramValue : String;
begin
  {zatvori otvoreni izvestaj}
  qReport.DisableControls;
  try
    qReport.Close;
  finally
    qReport.EnableControls;
  end;
  {kontrole su onemogucene!!!}
  paramValue:= '%' + edtFindProduct.Text + '%';
  {odredi tip parametra}
  qProduct.Parameters.ParamByName(paramName).DataType:= ftString;
  qProduct.Parameters.ParamByName(paramName).Value:= paramValue;
  try
    qProduct.Open;
  except
    on e : Exception do
    begin
      qProduct.CancelUpdates;
      qProduct.Close;
      qProduct.EnableControls; //obavezno!
      ShowMessage(e.Message);
      Exit;
    end;
  end;
  {omoguci kontrole}
  qProduct.EnableControls;
  {proveri rezultat}
  if qProduct.IsEmpty then
    begin
      edtFindProduct.SetFocus;
      edtFindProduct.Text:= '';
      Exit;
    end;
  {omoguci izbor}
  flowPanelProducts.Visible:= True;
  dbgProducts.SetFocus;
end;

procedure TfrmMain.runQueryReport;
const
  SUPPLIER_LOCATE_ERROR : String = 'Niste ispravno izabrali magacin(dobavljac).';
  locateStorageKey : String = 'M_NAZIV';
  EMPTY_SET_ERROR : String = 'Prazan skup podataka.';
begin
  if(not qStorages.Locate(locateStorageKey, cmbSupplier.Text, [loCaseInsensitive])) then
  begin
    ShowMessage(SUPPLIER_LOCATE_ERROR);
    cmbSupplier.SetFocus;
    Exit;
  end;
  {onemoguci kontrole i reopen-query}
  qReport.DisableControls;
  qReport.Close;

  try
    qReport.Open;
  finally
    qReport.EnableControls;
  end;
  {ima li slogova?}
  if qReport.IsEmpty then
  begin
    ShowMessage(EMPTY_SET_ERROR);
    edtFindProduct.SetFocus;
  end;
  {inace fokus na grid sa izvestajem}
  dbgReport.SetFocus;
end;

procedure TfrmMain.sbFindByClick(Sender: TObject);
begin
  PopupMenu1.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

end.
