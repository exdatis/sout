SELECT
  B.m_naziv,
  C.jm_naziv,
  sum(A.ka_minus)
FROM
  kartica_a A,
  magacin B,
  vproduct C
WHERE
  B.m_id = A.magacin_saradnik
  AND
  A.ka_magacin = 1
  AND
  A.ka_datum >= '2001-01-01'
  AND
  A.ka_datum <= '2015-12-31'
  AND
  A.ka_artikal = 325
  AND
  C.art_id = A.ka_artikal
GROUP BY
  B.m_naziv,
  C.jm_naziv